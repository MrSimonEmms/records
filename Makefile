PACKAGES_DIR = packages
TEMPLATE_DIR = template
PACKAGES = data ${PACKAGES_DIR}/* ${TEMPLATE_DIR}/*
TEMPLATE ?= mongoose-crud

build-package:
	rm -Rf ./tmp
	mkdir -p ./tmp/${PKG}

	cp -Rf ${TEMPLATE_DIR}/${TEMPLATE}/* ./tmp/${PKG}
	cp -Rf ./${PACKAGES_DIR}/${PKG}/* ./tmp/${PKG}/function
	rm -Rf ./tmp/**/node_modules
.PHONY: build

down:
	docker-compose down
.PHONY: down

install:
	npm ci
	npm run lerna -- ls
.PHONY: install

new-package:
	mkdir -p ${PACKAGES_DIR}

	cp -Rf ${TEMPLATE_DIR}/${TEMPLATE}/function ${PACKAGES_DIR}/${PKG}

	cat ${PACKAGES_DIR}/${PKG}/package.json | jq -r '.name="${OWNER}/${PKG}"' > tmp.json
	mv tmp.json ${PACKAGES_DIR}/${PKG}/package.json
	cat ${PACKAGES_DIR}/${PKG}/package-lock.json | jq -r '.name="${OWNER}/${PKG}"' > tmp.json
	mv tmp.json ${PACKAGES_DIR}/${PKG}/package-lock.json
	(cd ${PACKAGES_DIR}/${PKG} && npm ci)
.PHONY: new-package

run:
	docker-compose run \
		--rm \
		--service-ports \
		${SERVICE} \
		${CMD}
.PHONY: run

serve:
	docker-compose up
.PHONY: serve

uninstall:
	rm -Rf node_modules
	rm -Rf .git/hooks

	for dir in ${PACKAGES}; do \
		if [ -d $${dir} ]; then \
			(cd $${dir} && rm -Rf node_modules); \
		fi; \
  	done
.PHONY: uninstall
