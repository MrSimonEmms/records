module.exports = () => [
  {
    _id: 'org1-id',
    name: 'Org 1',
    slug: 'org1',
    users: [
      {
        role: 'ORG_MAINTAINER',
        userId: '6b854023-0897-419f-b911-a783df572b48',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    __v: 0,
  },
  {
    _id: 'org2-id',
    name: 'Org 2',
    slug: 'org2',
    users: [
      // {
      //   role: 'ORG_MAINTAINER',
      //   userId: '6b854023-0897-419f-b911-a783df572b48',
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
    ],
    __v: 0,
  },
];
