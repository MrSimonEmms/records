const fastify = require('fastify');
const metricsPlugin = require('fastify-metrics');
const crudRequest = require('@nestjsx/crud-request');
const uuid = require('uuid');

const config = require('./config');
const mongooseBootstrap = require('./lib/mongoose');
const BaseController = require('./lib/controller');
const BaseRoute = require('./lib/routes');
const BaseSchema = require('./lib/schema');
const schema = require('../function/schema');
const utils = require('./lib/utils');

async function loadFunctionFile(logger, filePath) {
  try {
    const { default: file } = await import(filePath);

    return file;
  } catch (err) {
    logger.trace({ err }, 'File not found');
    if (err.code !== 'ERR_MODULE_NOT_FOUND') {
      throw err;
    }

    return undefined;
  }
}

async function loadClass(logger, BaseClass, filePath) {
  const factory = await loadFunctionFile(logger, filePath);

  if (factory) {
    logger.trace({ filePath }, 'Loading custom and base class');
    return factory(BaseClass);
  }

  return BaseClass;
}

async function main(app) {
  const mongoose = await mongooseBootstrap(app.log);

  const Schema = await schema(mongoose, BaseSchema, { config });

  app.log.info('Registering middleware');

  const middleware = await loadFunctionFile(app.log, '../function/middleware.js');

  if (middleware) {
    app.log.info('Registering middleware');
    await middleware(app, config);
  }

  const Routes = await loadClass(app.log, BaseRoute, '../function/routes.js');
  const Controller = await loadClass(app.log, BaseController, '../function/controller.js');

  const controller = new Controller(config, crudRequest, utils);
  const routes = new Routes(config, controller, { mongoose, Schema });

  /* Health is a special route */
  app.get(
    '/health',
    routes.handlerFactory((...args) => controller.health(...args)),
  );
  if (config.metrics.enabled) {
    app.log.debug('Metrics enabled');
    app.register(metricsPlugin, {
      enableDefaultMetrics: config.metrics.defaultMetricsEnabled,
      enableRouteMetrics: config.metrics.routeMetricsEnabled,
      endpoint: '/metrics',
      interval: config.metrics.interval,
    });
  } else {
    app.log.debug('Metrics not enabled');
  }

  app.register((...args) => routes.register(...args));

  await app.listen(config.server.port, config.server.host);

  app.log.info({ config }, 'Server started');
}

const app = fastify({
  logger: {
    level: config.logger.level,
    redact: config.logger.redact,
  },
  requestIdHeader: config.logger.requestIdHeader,
  genReqId: () => uuid.v4(),
});

main(app).catch((err) => {
  app.log.error({ err }, 'Failed to start server');
  process.exit(1);
});
