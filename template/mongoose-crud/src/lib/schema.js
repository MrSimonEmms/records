const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const uuid = require('uuid');

module.exports = class BaseSchema extends Schema {
  constructor(schema, opts = {}) {
    super(schema, {
      timestamps: true,
      ...opts,
    });

    this.add({
      _id: {
        type: String,
        default: () => uuid.v4(),
      },
    });

    if (opts.timestamps !== false) {
      this.index({
        createdAt: 1,
      });
    }

    ['toJSON', 'toObject'].forEach((path) => {
      this.set(path, {
        virtuals: true,
        versionKey: false,
        transform: (doc, ret) => ({
          ...ret,
          _id: undefined,
        }),
      });
    });

    this.plugin(mongoosePaginate);
  }
};
