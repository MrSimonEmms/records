const schemaFactory = require('./schema');

const modelName = 'User';

describe('Schema test', () => {
  let schema;
  let mongoose;
  let BaseSchema;
  const validators = {};
  beforeEach(() => {
    mongoose = {
      model: jest.fn(),
    };
    BaseSchema = jest.fn();
    schemaFactory(mongoose, BaseSchema);

    const schemaFactoryCalls = mongoose.model.mock.calls[0];
    expect(schemaFactoryCalls[0]).toBe(modelName);
    [[schema]] = BaseSchema.mock.calls;

    /* Extract the validators */
    Object.keys(schema).forEach((key) => {
      const value = schema[key];

      if (value.validate) {
        validators[key] = value.validate;
        delete value.validate;
      }
    });

    expect(Object.keys(validators)).toEqual(['emailAddress']);
  });

  it('should define the schema', () => {
    expect(schema).toEqual({
      name: {
        type: String,
        required: true,
      },
      emailAddress: {
        type: String,
        required: true,
        unique: true,
      },
    });
  });

  describe('validation', () => {
    describe('#emailAddress', () => {
      it('should error if invalid email', async () => {
        try {
          await validators.emailAddress.validator('invalid');
          expect('Error').toBeUndefined();
        } catch (err) {
          expect(err).toBeInstanceOf(Error);
          expect(err.message).toBe('Value is an invalid email address');
        }
      });

      it('should simulate error if email found in database', async () => {
        const userId = 'some-id';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue('some value'),
          where: jest.fn().mockReturnThis(),
        };

        const emailAddress = 'test@test.com';

        try {
          await validators.emailAddress.validator.call(context, emailAddress);
          expect('Error').toBeUndefined();
        } catch (err) {
          expect(err).toBeInstanceOf(Error);
          expect(err.message).toBe('Email address already registered');

          expect(context.model).toBeCalledWith(modelName);
          expect(context.findOne).toBeCalledWith({ emailAddress });
          expect(context.where).toBeCalledWith('_id');
          expect(context.nin).toBeCalledWith([userId]);
        }
      });

      it('should simulate pass if email not found in database', async () => {
        const userId = 'some-id2';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue(null),
          where: jest.fn().mockReturnThis(),
        };

        const emailAddress = 'test2@test.com';

        expect(await validators.emailAddress.validator.call(context, emailAddress)).toBe(true);

        expect(context.model).toBeCalledWith(modelName);
        expect(context.findOne).toBeCalledWith({ emailAddress });
        expect(context.where).toBeCalledWith('_id');
        expect(context.nin).toBeCalledWith([userId]);
      });
    });
  });
});
