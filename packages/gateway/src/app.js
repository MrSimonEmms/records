const express = require('express');
const expressPinoLogger = require('express-pino-logger');
const uuid = require('uuid');
require('express-async-errors');

const config = require('./config');
const logger = require('./logger');
const routes = require('./routes');

express()
  .disable('x-powered-by')
  .use(
    expressPinoLogger({
      logger,
      genReqId: (req) => req.headers[config.logger.requestIdHeader] ?? uuid.v4(),
    }),
  )
  .use((req, res, next) => {
    req.log.info('Incoming request');
    next();
  })
  .use(routes)
  // eslint-disable-next-line no-unused-vars
  .use((err, req, res, next) => {
    req.log.error({ err }, 'General error');
    res.status(500).send({
      message: err?.message ?? 'General error',
    });
  })
  .listen(config.server.port, () => {
    logger.info({ config }, 'Listening');
  });
