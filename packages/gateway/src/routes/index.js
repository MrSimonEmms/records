const { Router } = require('express');
const proxyFactory = require('../lib/proxyFactory');
const config = require('../config');
const api = require('./api');

module.exports = new Router({ mergeParams: true })
  .use('/api', api)
  /* As final catch-all, send to website */
  .use(...proxyFactory(config.services.website));
