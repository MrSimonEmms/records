const { Router } = require('express');
const proxyFactory = require('../../../lib/proxyFactory');
const config = require('../../../config');
const organization = require('./organization');

module.exports = new Router({ mergeParams: true })
  .use('/organization', organization)
  .use(
    ...proxyFactory(config.services.provider, {
      url: '/provider',
    }),
  )
  .use(
    ...proxyFactory(config.services.role, {
      url: '/role',
    }),
  )
  .use(
    ...proxyFactory(config.services.user, {
      url: '/user',
    }),
  );
