const { Router } = require('express');
const proxyFactory = require('../../../../lib/proxyFactory');
const config = require('../../../../config');
const apiary = require('./apiary');

module.exports = new Router({ mergeParams: true }).use('/:orgId/apiary', apiary).use(
  ...proxyFactory(config.services.organization, {
    customPathRewrite: { '^/api/v1/organization': '' },
  }),
);
