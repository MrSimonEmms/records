const { Router } = require('express');
const crudFactory = require('../../../../../lib/crudFactory');
const middleware = require('../../../../../middleware');
const config = require('../../../../../config');

const route = new Router({ mergeParams: true })
  .use(middleware.auth)
  .use((req, res, next) => middleware.userInOrg(req.params.orgId)(req, res, next))
  .use((req, res, next) => {
    const possession = req.orgUser ? 'own' : 'all';

    middleware.role({ possession, resource: 'ORGANIZATION' })(req, res, next);
  });

crudFactory(route, {
  customPathRewrite: { '^/api/v1/organization/.*/apiary': '' },
  getManyFilter: (searchParams, path, req) => {
    searchParams.append('filter', `organizationId||$eq||${req.params.orgId}`);
  },
  route: config.services.apiary,
});

module.exports = route;
