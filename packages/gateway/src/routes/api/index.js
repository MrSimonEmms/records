const { STATUS_CODES } = require('http');
const { Router } = require('express');
const v1 = require('./v1');

module.exports = new Router({ mergeParams: true })
  .use('/v1', v1)
  /* Catch-all for all bad API calls */
  .use((req, res) => {
    req.log.debug('Unknown API endpoint called - returning 404');

    const statusCode = 404;

    res.status(statusCode).send({ statusCode, message: STATUS_CODES[statusCode] });
  });
