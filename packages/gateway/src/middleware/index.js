const auth = require('./auth');
const role = require('./role');
const userInOrg = require('./userInOrg');

module.exports = {
  auth,
  role,
  userInOrg,
};
