const axios = require('axios');
const config = require('../config');

module.exports = (organizationId) => async (req, res, next) => {
  if (!req.user) {
    req.log.debug('Not logged in - not continuing to find organization');
    next();
    return;
  }

  const { id: userId } = req.user;
  req.log.info({ organizationId, userId }, 'Search for user in organization');

  try {
    const { data } = await axios({
      method: 'get',
      baseURL: config.services.organization,
      url: organizationId,
      headers: req.headers,
    });

    req.organization = data;

    /* Failsafe - the application returns 404 if not in user list */
    const orgUser = req.organization.users.find(({ userId: id }) => userId === id);

    if (orgUser) {
      req.log.debug('User found in organization');

      req.orgUser = orgUser;
    } else {
      req.log.warn('User not in organization');
    }
  } catch (err) {
    req.log.warn({ err }, 'Failed to find organization - continuing');
  }

  next();
};
