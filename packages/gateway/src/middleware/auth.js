const { STATUS_CODES } = require('http');
const axios = require('axios');
const config = require('../config');

module.exports = async (req, res, next) => {
  const method = 'get';
  const url = config.services.user;

  req.log.info(
    {
      method,
      url,
    },
    'Authenticating user',
  );

  try {
    const { data } = await axios({
      method,
      url,
      headers: {
        authorization: req.headers?.authorization ?? '',
      },
    });

    req.headers.userId = data.id;
    req.user = data;

    req.log.debug('Authentication successfully called');

    next();
  } catch (err) {
    req.log.error({ err }, 'Failed to authenticate user');

    const statusCode = 401;

    res.status(statusCode).send({ statusCode, message: STATUS_CODES[statusCode] });
  }
};
