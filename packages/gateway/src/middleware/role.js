const { STATUS_CODES } = require('http');
const axios = require('axios');
const config = require('../config');

module.exports =
  ({ resource, possession = 'all' }) =>
  async (req, res, next) => {
    const method = 'post';
    const url = config.services.role;

    try {
      let action;
      switch (req.method.toUpperCase()) {
        case 'POST':
          action = 'create';
          break;

        case 'GET':
          action = 'read';
          break;

        case 'PATCH':
        case 'PUT':
          action = 'update';
          break;

        case 'DELETE':
          action = 'read';
          break;

        default:
          action = null;
          break;
      }

      const body = {
        roles: [
          {
            action,
            resource,
            possession,
          },
        ],
      };

      req.log.info(
        {
          method,
          url,
          data: body,
        },
        'Verify user access to role',
      );

      const { data } = await axios({
        method,
        url,
        data: body,
        headers: {
          authorization: req.headers?.authorization ?? '',
        },
      });

      req.headers['x-roles'] = data.userRoles.join(',');
      req.user = {
        ...req.user,
        roles: data.userRoles,
      };
      next();
    } catch (err) {
      req.log.error({ err }, 'User denied access to resource');

      const statusCode = 403;

      res.status(statusCode).send({ statusCode, message: STATUS_CODES[statusCode] });
    }
  };
