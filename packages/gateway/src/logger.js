const pino = require('pino');
const config = require('./config');

module.exports = pino({
  level: config.logger.level,
  redact: config.logger.redact,
});
