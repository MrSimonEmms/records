const loggerRedact = (process.env.LOGGER_REDACT ?? '')
  .split(',')
  .map((item) => item.trim())
  .filter((item) => item);

module.exports = {
  logger: {
    level: process.env.LOGGER_LEVEL ?? 'info',
    redact: [...loggerRedact],
    requestIdHeader: process.env.REQUEST_ID_HEADER ?? 'x-correlation-id',
  },
  server: {
    host: '0.0.0.0',
    port: Number(process.env.http_port ?? 3000), // Use http_port for compatibility with OpenFaaS watchdog
  },
  services: {
    apiary: `${process.env.APIARY_SERVICE_URL}/crud`,
    organization: `${process.env.AUTH_SERVICE_URL}/organization`,
    provider: `${process.env.AUTH_SERVICE_URL}/provider`,
    role: `${process.env.AUTH_SERVICE_URL}/role`,
    user: `${process.env.AUTH_SERVICE_URL}/user`,
    website: process.env.WWW_SERVICE_URL,
  },
};
