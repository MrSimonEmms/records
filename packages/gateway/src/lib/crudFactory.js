const { URL } = require('url');
const proxyFactory = require('./proxyFactory');

function mergeSearchParams(fn) {
  return (path, req) => {
    const { searchParams } = new URL(path, 'http://localhost');

    if (fn) {
      fn(searchParams, path, req);
    }

    const queryString = searchParams.toString();

    let outputPath = '';
    if (queryString) {
      outputPath += `?${queryString}`;
    }

    return outputPath;
  };
}

module.exports = (router, opts = {}) => {
  const { customPathRewrite, getManyFilter, middleware = [], route = null, routes = {} } = opts;

  const {
    getMany = true,
    getOne = true,
    createOne = true,
    deleteOne = true,
    updateOne = true,
  } = routes;

  if (route === null) {
    throw new Error('Route must be set');
  }

  if (getMany) {
    router.get(
      '/',
      ...proxyFactory(route, {
        middleware,
        customPathRewrite: mergeSearchParams(getManyFilter),
      }),
    );
  }
  if (createOne) {
    router.post(
      '/',
      ...proxyFactory(route, {
        customPathRewrite,
        middleware,
      }),
    );
  }
  if (getOne) {
    router.get(
      '/:id',
      ...proxyFactory(route, {
        customPathRewrite,
        middleware,
      }),
    );
  }
  if (deleteOne) {
    router.delete(
      '/:id',
      ...proxyFactory(route, {
        customPathRewrite,
        middleware,
      }),
    );
  }
  if (updateOne) {
    router.patch(
      '/:id',
      ...proxyFactory(route, {
        customPathRewrite,
        middleware,
      }),
    );
  }

  return router;
};
