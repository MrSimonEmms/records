const { createProxyMiddleware } = require('http-proxy-middleware');
const config = require('../config');
const logger = require('../logger');

function logFactory(level) {
  return (...args) => logger[level](...args);
}

module.exports = (target, { middleware = [], customPathRewrite = {}, url = undefined } = {}) => {
  const response = [];

  let pathRewrite;

  if (typeof customPathRewrite === 'function') {
    pathRewrite = customPathRewrite;
  } else {
    pathRewrite = {
      ...customPathRewrite,
    };

    if (url) {
      const pathRewriteUrl = url
        .split('/')
        .map((item) => (item.startsWith(':') ? '(.*)' : item))
        .join('/');

      response.push(url);
      pathRewrite[`^(.*)${pathRewriteUrl}`] = '';
    }
  }

  if (middleware) {
    response.push(...middleware);
  }

  response.push(
    createProxyMiddleware({
      target,
      changeOrigin: true,
      pathRewrite,
      logLevel: 'debug', // Let main logger handle this
      logProvider: () => ({
        log: logFactory('info'),
        debug: logFactory('debug'),
        info: logFactory('info'),
        warn: logFactory('warn'),
        error: logFactory('error'),
      }),
      onProxyReq(proxyReq, req) {
        proxyReq.setHeader(config.logger.requestIdHeader, req.id);
      },
    }),
  );

  return response;
};
