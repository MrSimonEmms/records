module.exports = ({ model }, BaseSchema) => {
  const modelName = 'Apiary';

  const UserSchema = new BaseSchema({
    name: {
      type: String,
      required: true,
    },
    organizationId: {
      type: String,
      required: true,
    },
  });

  return model(modelName, UserSchema);
};
